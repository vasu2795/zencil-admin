use Mix.Config

# In this file, we keep production configuration that
# you likely want to automate and keep it away from
# your version control system.
#
# You should document the content of this
# file or create a script for recreating it, since it's
# kept out of version control and might be hard to recover
# or recreate for your teammates (or you later on).
config :admin_zencil, ZencilAdmin.Endpoint,
  secret_key_base: "L2wmX+KTG2lUvnbSczvo9FK0X9NCcCkx2Kr+lJvVJo1KHRx8wad9jlRKycutPy9t"

# Configure your database
config :admin_zencil, ZencilAdmin.Repo,
  adapter: Ecto.Adapters.Postgres,
  username: "postgres",
  password: "postgres",
  database: "admin_zencil_prod",
  pool_size: 20
