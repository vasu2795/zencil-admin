# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :zencil_admin,
  ecto_repos: [ZencilAdmin.Repo]

# Configures the endpoint
config :zencil_admin, ZencilAdmin.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "Y+XntCR1rjj9q4dUGn/Lhm6hQmBTof++cB+CKco0LE/x+b+wiYNXXKikC6KY0Ir/",
  render_errors: [view: ZencilAdmin.ErrorView, accepts: ~w(html json)],
  pubsub: [name: ZencilAdmin.PubSub,
           adapter: Phoenix.PubSub.PG2],
  watchers: [npm: ["run", "watch"]]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"
