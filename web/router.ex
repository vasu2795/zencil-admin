defmodule ZencilAdmin.Router do
  use ZencilAdmin.Web, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :auth do
    plug :accepts, ["json"]
    plug :fetch_session
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", ZencilAdmin do
    pipe_through :browser # Use the default browser stack

    get "/", PageController, :index
  end

  scope "/auth", ZencilAdmin do
    pipe_through :auth

    post "/login", AuthController, :login
  end

  scope "/", ZencilAdmin do
    pipe_through :api
    get "/published_posts", PostController, :list_published_posts
    get "/pending_posts", PostController, :list_pending_posts
    get "/deleted_posts", PostController, :list_deleted_posts
    get "/discarded_posts", PostController, :list_discarded_posts
    get "/users", UserController, :list_users

    get "/published_posts/:id", PostController, :get_post
    get "/pending_posts/:id", PostController, :get_post
    get "/deleted_posts/:id", PostController, :get_post
    get "/discarded_posts/:id", PostController, :get_post
    get "/users/:id", UserController, :get_user

    delete "/published_posts/:id", PostController, :delete_post
    delete "/pending_posts/:id", PostController, :discard_post

    put "/approve/:id", PostController, :approve_post

  end
  # Other scopes may use custom stacks.
  # scope "/api", ZencilAdmin do
  #   pipe_through :api
  # end
end
