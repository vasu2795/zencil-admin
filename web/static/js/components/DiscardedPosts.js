import React from 'react';
import { List,Edit,SimpleForm,DisabledInput,ReferenceInput, SelectInput, RadioButtonGroupInput,TextInput,LongTextInput,Datagrid, TextField,EditButton } from 'admin-on-rest/lib/mui';
import { CardActions } from 'material-ui/Card';
import FlatButton from 'material-ui/FlatButton';

import { ListButton, ShowButton, DeleteButton, ChipField } from 'admin-on-rest/lib/mui';
import ActionVisibility from 'material-ui/svg-icons/action/visibility';
import ApproveButton from './ApproveButton';

const cardActionStyle = {
    zIndex: 2,
    display: 'inline-block',
    float: 'right',
};

const DiscardedPostEditActions = ({ basePath, data, refresh }) => (
    <CardActions style={cardActionStyle}>
        <ApproveButton label="Publish" basePath={basePath} record={data} />
        <ListButton basePath={basePath} />
        <FlatButton primary label="Refresh" onClick={refresh}   />
        <FlatButton primary  label="Preview" onClick={() => { console.log("clicking",data);  window.open(`#/preview/${data.id}`,'_blank');}} ></FlatButton>
    </CardActions>
);

export const DiscardedPostList = (props) => (
    <List {...props}>
        <Datagrid>
            <TextField source="id" />
            <TextField source="title" />
            <ChipField label="Category" source="category" />
            <TextField label="Author" source="username" />
            <EditButton />
        </Datagrid>
    </List>
);

const DiscardedPostTitle = ({ record }) => {
    return <span>DiscardedPost {record ? `"${record.title}"` : ''}</span>;
};

export const DiscardedPostEdit = (props) => (
    <Edit title={<DiscardedPostTitle />} actions={<DiscardedPostEditActions />} {...props}>
        <SimpleForm>
            <DisabledInput source="id" />
            <DisabledInput source="title" />
            <DisabledInput source="username" />
            <DisabledInput label = "Discarded At" source="last_updated" />
        </SimpleForm>
    </Edit>
);
