import React from 'react';
import { Filter,List,Edit,SimpleForm,DisabledInput,ReferenceInput, SelectInput, RadioButtonGroupInput,TextInput,LongTextInput,Datagrid, TextField,EditButton } from 'admin-on-rest/lib/mui';
import { CardActions } from 'material-ui/Card';
import FlatButton from 'material-ui/FlatButton';

import { ListButton, ShowButton, DeleteButton, ChipField } from 'admin-on-rest/lib/mui';
import ActionVisibility from 'material-ui/svg-icons/action/visibility';
import ApproveButton from './ApproveButton';

const cardActionStyle = {
    zIndex: 2,
    display: 'inline-block',
    float: 'right',
};


const PublishedPostEditActions = ({ basePath, data, refresh }) => (
    <CardActions style={cardActionStyle}>
        <ListButton basePath={basePath} />
        <DeleteButton  basePath={basePath} record={data} />
        <FlatButton primary label="Refresh" onClick={refresh}   />
        <FlatButton primary  label="Preview" onClick={() => { console.log("clicking",data);  window.open(`#/preview/${data.id}`,'_blank');}} ></FlatButton>
    </CardActions>
);

export const PublishedPostList = (props) => (
    <List {...props}  >
        <Datagrid>
            <TextField source="id" />
            <TextField source="title" />
            <ChipField label="Category" source="category" />
            <TextField label="Author" source="username" />
            <EditButton />
        </Datagrid>
    </List>
);

const PublishedPostTitle = ({ record }) => {
    return <span>PublishedPost {record ? `"${record.title}"` : ''}</span>;
};

export const PublishedPostEdit = (props) => (
    <Edit title={<PublishedPostTitle />} actions={<PublishedPostEditActions />} {...props}>
        <SimpleForm>
            <DisabledInput source="id" />
            <DisabledInput source="title" />
            <DisabledInput source="username" />
            <DisabledInput label = "Published At" source="last_updated" />
            <RadioButtonGroupInput source="category" choices={[
                { id: 'programming', name: 'Programming' },
                { id: 'lifestyle', name: 'Lifestyle' },
                { id: 'photography', name: 'Photography' },
            ]} />
        </SimpleForm>
    </Edit>
);
