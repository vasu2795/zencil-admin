import React from 'react';

export default class LogoutMessage extends React.Component {
  constructor(props) {
    super(props);
    this.props = props;
  }

  render() {
    return (
      <div className="ui green segment">
        You have been sucessfully logged out.
        <a className="ui green button" href="/login">Click here to Login</a>
      </div>
    );
  }
}
