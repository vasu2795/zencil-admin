import React from 'react';
import { List,Edit,SimpleForm,DisabledInput,ReferenceInput, SelectInput, RadioButtonGroupInput,TextInput,LongTextInput,Datagrid, TextField,EditButton } from 'admin-on-rest/lib/mui';
import { CardActions } from 'material-ui/Card';
import FlatButton from 'material-ui/FlatButton';

import { ListButton, ShowButton, DeleteButton, ChipField } from 'admin-on-rest/lib/mui';
import ActionVisibility from 'material-ui/svg-icons/action/visibility';
import ApproveButton from './ApproveButton';

const cardActionStyle = {
    zIndex: 2,
    display: 'inline-block',
    float: 'right',
};

const PendingPostEditActions = ({ basePath, data, refresh }) => (
    <CardActions style={cardActionStyle}>
        <ApproveButton basePath={basePath} record={data} />
        <ListButton basePath={basePath} />
        <DeleteButton label= "Discard" basePath={basePath} record={data} />
        <FlatButton primary label="Refresh" onClick={refresh}   />
        <FlatButton primary  label="Preview" onClick={() => { console.log("clicking",data);  window.open(`#/preview/${data.id}`,'_blank');}} ></FlatButton>
    </CardActions>
);

export const PendingPostList = (props) => (
    <List {...props}>
        <Datagrid>
            <TextField source="id" />
            <TextField source="title" />
            <ChipField label="Category" source="category" />
            <TextField label="Author" source="username" />
            <EditButton />
        </Datagrid>
    </List>
);

const PendingPostTitle = ({ record }) => {
    return <span>PendingPost {record ? `"${record.title}"` : ''}</span>;
};

export const PendingPostEdit = (props) => (
    <Edit title={<PendingPostTitle />} actions={<PendingPostEditActions />} {...props}>
        <SimpleForm>
            <DisabledInput source="id" />
            <DisabledInput source="title" />
            <DisabledInput source="username" />
            <DisabledInput label = "Pending At" source="last_updated" />
            <RadioButtonGroupInput source="category" choices={[
                { id: 'programming', name: 'Programming' },
                { id: 'lifestyle', name: 'Lifestyle' },
                { id: 'photography', name: 'Photography' },
            ]} />
        </SimpleForm>
    </Edit>
);
