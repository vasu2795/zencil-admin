import React from 'react';
import R from 'ramda';

import dispatchNavigationEvent from '../RoutingManagement';

export default class MainApp extends React.Component {
  constructor(props) {
    super(props);

      }
  //
  // prepareNavigationItems() {
  //   let roles = JSON.parse(sessionStorage.getItem('roles'));
  //   let merchantId = sessionStorage.getItem('merchant_id');
  //   let resellerId = sessionStorage.getItem('reseller_id');
  //
  //   let items = [];
  //
  //   if(R.contains('role_admin', roles) && R.contains('role_switch_user', roles)) {
  //     let adminItems = [{
  //         category: 'Resellers',
  //         regex: /^resellers\/list$/i,
  //         template: `resellers / list`,
  //         description: 'List'
  //       }, {
  //         category: 'Resellers',
  //         regex: /^resellers\/new$/i,
  //         template: `resellers / new`,
  //         description: 'Create'
  //       }, {
  //         category: 'Resellers',
  //         regex: /^resellers\/(\S+)\/summary$/i,
  //         expect: [':reseller_id'],
  //         template: 'resellers / :reseller_id / summary',
  //         description: 'Summary'
  //       }, {
  //         category: 'Merchants',
  //         regex: /^merchants\/list$/i,
  //         template: 'merchants / list',
  //         description: 'List'
  //       }, {
  //         category: 'Merchants',
  //         regex: /^merchants\/create$/i,
  //         template: 'merchants / create',
  //         description: 'Create'
  //       }, {
  //         category: 'Merchants',
  //         regex: /^merchants\/(\S+)\/summary$/i,
  //         template: 'merchants / :merchant_id / summary',
  //         expect: [':merchant_id'],
  //         selectionRange: [12, 24],
  //         description: 'View'
  //       }];
  //
  //     items.push(...adminItems);
  //   }
  //
  //   if(R.contains('role_merchant_admin', roles)) {
  //     let merchantAdminItems = [{
  //         category: 'Orders',
  //         regex: /^merchants\/(\S+)\/orders$/i,
  //         selectionRange:[12, 24],
  //         template: `merchants / ${merchantId} / orders`,
  //         description: 'List'
  //       }, {
  //         category: 'Orders',
  //         regex: /^merchants\/(\S+)\/orders\/(\S+)$/i,
  //         expect: [':order_id'],
  //         selectionRange:[12, 24],
  //         template: `merchants / ${merchantId} / orders / :order_id`,
  //         description: 'Search'
  //       }, {
  //         category: 'Refunds',
  //         regex: /^merchants\/(\S+)\/batch_refunds$/i,
  //         selectionRange:[12, 24],
  //         template: `merchants / ${merchantId} / batch_refunds`,
  //         description: 'List / Create refunds'
  //       }, {
  //         category: 'Refunds',
  //         regex: /^merchants\/(\S+)\/batch_refunds\/(\S+)$/i,
  //         expect: [':batch_id'],
  //         template: `merchants / ${merchantId} / batch_refunds / :batch_id`,
  //         description: 'View refund status'
  //       }, {
  //         category: 'Customers',
  //         regex: /^merchants\/(\S+)\/customers$/i,
  //         template: `merchants / ${merchantId} / customers`,
  //         description: 'List'
  //       }, {
  //         category: 'Customers',
  //         regex: /^merchants\/(\S+)\/customers\/new$/i,
  //         template: `merchants / ${merchantId} / customers / new`,
  //         description: 'Create'
  //       }, {
  //         category: 'Manage Checkout',
  //         regex: /^merchants\/(\S+)\/checkout\/theme$/i,
  //         template: `merchants / ${merchantId} / checkout / theme`,
  //         description: 'Theme'
  //       }, {
  //         category: 'Manage Checkout',
  //         regex: /^merchants\/(\S+)\/checkout\/mobile-web$/i,
  //         template: `merchants / ${merchantId} / checkout / mobile-web`,
  //         description: 'Mobile Web'
  //       }, {
  //         category: 'Account',
  //         regex: /^merchants\/(\S+)\/summary$/i,
  //         template: `merchants / ${merchantId} / summary`,
  //         description: 'Profile'
  //       }, {
  //         category: 'Account',
  //         regex: /^merchants\/(\S+)\/conflict$/i,
  //         template: `merchants / ${merchantId} / conflict`,
  //         description: 'Conflict'
  //       }, {
  //         category: 'Account',
  //         regex: /^merchants\/(\S+)\/webhooks$/i,
  //         template: `merchants / ${merchantId} / webhooks`,
  //         description: 'Webhooks'
  //       }, {
  //         category: 'Security',
  //         regex: /^merchants\/(\S+)\/security\/api_keys$/i,
  //         template: `merchants / ${merchantId} / security / api_keys`,
  //         description: 'API Keys'
  //       }, {
  //         category: 'Security',
  //         regex: /^merchants\/(\S+)\/security\/ingress_rules$/i,
  //         template: `merchants / ${merchantId} / security / ingress_rules`,
  //         description: 'IP Whitelisting'
  //       }, {
  //         category: 'Gateways',
  //         regex: /^merchants\/(\S+)\/gateways$/i,
  //         selectionRange: [12, 24],
  //         template: `merchants / ${merchantId} / gateways`,
  //         description: 'List'
  //       },{
  //         category: 'Gateways',
  //         regex: /^merchants\/(\S+)\/gateways\/(\S+)\/configure$/i,
  //         expect: [':gateway_id'],
  //         template: `merchants / ${merchantId} / gateways / :gateway_id / configure`,
  //         description: 'New Gateway'
  //       }, {
  //         category: 'Gateways',
  //         regex: /^merchants\/(\S+)\/gateways\/([0-9]{1})\/settings$/i,
  //         expect: [':gateway_id'],
  //         selectionRange: [12, 24],
  //         template: `merchants / ${merchantId} / gateways / :gateway_id / settings`,
  //         description: 'View'
  //       }, {
  //         category: 'Gateways',
  //         regex: /^merchants\/(\S+)\/gateways\/logic$/i,
  //         template: `merchants / ${merchantId} / gateways / logic`,
  //         description: 'Priority Logic'
  //       }, {
  //         category: 'Operators',
  //         regex: /^merchants\/(\S+)\/users$/i,
  //         template: `merchants / ${merchantId} / users`,
  //         description: 'List'
  //       }, {
  //         category: 'Operators',
  //         regex: /^merchants\/(\S+)\/users$/i,
  //         template: `merchants / ${merchantId} / users`,
  //         description: 'New'
  //       }];
  //
  //     items.push(...merchantAdminItems);
  //   }
  //
  //   if(R.contains('role_user', roles)) {
  //     let userItems = [{
  //         category: 'Orders',
  //         regex: /^merchants\/(\S+)\/orders$/i,
  //         selectionRange:[12, 24],
  //         template: `merchants / ${merchantId} / orders`,
  //         description: 'List'
  //       }, {
  //         category: 'Orders',
  //         regex: /^merchants\/(\S+)\/orders\/(\S+)$/i,
  //         expect: [':order_id'],
  //         selectionRange:[12, 24],
  //         template: `merchants / ${merchantId} / orders / :order_id`,
  //         description: 'Search'
  //       }];
  //
  //     items.push(...userItems);
  //   }
  //
  //   if(R.contains('role_merchant', roles)) {
  //     let merchantItems = [{
  //         category: 'Orders',
  //         regex: /^merchants\/(\S+)\/orders$/i,
  //         selectionRange:[12, 24],
  //         template: `merchants / ${merchantId} / orders`,
  //         description: 'List'
  //       }, {
  //         category: 'Orders',
  //         regex: /^merchants\/(\S+)\/orders\/(\S+)$/i,
  //         expect: [':order_id'],
  //         selectionRange:[12, 24],
  //         template: `merchants / ${merchantId} / orders / :order_id`,
  //         description: 'Search'
  //       }, {
  //         category: 'Refunds',
  //         regex: /^merchants\/(\S+)\/batch_refunds$/i,
  //         selectionRange:[12, 24],
  //         template: `merchants / ${merchantId} / batch_refunds`,
  //         description: 'List / Create refunds'
  //       }, {
  //         category: 'Refunds',
  //         regex: /^merchants\/(\S+)\/batch_refunds\/(\S+)$/i,
  //         expect: [':batch_id'],
  //         template: `merchants / ${merchantId} / batch_refunds / :batch_id`,
  //         description: 'View refund status'
  //       }, {
  //         category: 'Customers',
  //         regex: /^merchants\/(\S+)\/customers$/i,
  //         template: `merchants / ${merchantId} / customers`,
  //         description: 'List'
  //       }, {
  //         category: 'Customers',
  //         regex: /^merchants\/(\S+)\/customers\/new$/i,
  //         template: `merchants / ${merchantId} / customers / new`,
  //         description: 'Create'
  //       }, {
  //         category: 'Manage Checkout',
  //         regex: /^merchants\/(\S+)\/checkout\/theme$/i,
  //         template: `merchants / ${merchantId} / checkout / theme`,
  //         description: 'Theme'
  //       }, {
  //         category: 'Manage Checkout',
  //         regex: /^merchants\/(\S+)\/checkout\/mobile-web$/i,
  //         template: `merchants / ${merchantId} / checkout / mobile-web`,
  //         description: 'Mobile Web'
  //       }, {
  //         category: 'Account',
  //         regex: /^merchants\/(\S+)\/summary$/i,
  //         template: `merchants / ${merchantId} / summary`,
  //         description: 'Profile'
  //       }, {
  //         category: 'Account',
  //         regex: /^merchants\/(\S+)\/conflict$/i,
  //         template: `merchants / ${merchantId} / conflict`,
  //         description: 'Conflict'
  //       }, {
  //         category: 'Account',
  //         regex: /^merchants\/(\S+)\/webhooks$/i,
  //         template: `merchants / ${merchantId} / webhooks`,
  //         description: 'Webhooks'
  //       }, {
  //         category: 'Security',
  //         regex: /^merchants\/(\S+)\/security\/api_keys$/i,
  //         template: `merchants / ${merchantId} / security / api_keys`,
  //         description: 'API Keys'
  //       }, {
  //         category: 'Security',
  //         regex: /^merchants\/(\S+)\/security\/ingress_rules$/i,
  //         template: `merchants / ${merchantId} / security / ingress_rules`,
  //         description: 'IP Whitelisting'
  //       }, {
  //         category: 'Operators',
  //         regex: /^merchants\/(\S+)\/users$/i,
  //         template: `merchants / ${merchantId} / users`,
  //         description: 'List'
  //       }, {
  //         category: 'Operators',
  //         regex: /^merchants\/(\S+)\/users$/i,
  //         template: `merchants / ${merchantId} / users`,
  //         description: 'New'
  //       }];
  //
  //     items.push(...merchantItems);
  //   }
  //
  //   if(R.contains('role_reseller', roles)) {
  //     let resellerItems  = [{
  //       category: 'Merchants',
  //       regex: /^merchants\/list$/i,
  //       template: 'merchants / list',
  //       description: 'List'
  //     }, {
  //       category: 'Merchants',
  //       regex: /^merchants\/create$/i,
  //       template: 'merchants / create',
  //       description: 'Create'
  //     }, {
  //       category: 'Merchants',
  //       regex: /^merchants\/(\S+)\/summary$/i,
  //       template: 'merchants / :merchant_id / summary',
  //       expect: [':merchant_id'],
  //       selectionRange: [12, 24],
  //       description: 'View'
  //     }, {
  //       category: 'Orders',
  //       regex: /^merchants\/(\S+)\/orders$/i,
  //       expect: [':merchant_id'],
  //       selectionRange:[12, 24],
  //       template: 'merchants / :merchant_id / orders',
  //       description: 'List'
  //     }, {
  //       category: 'Orders',
  //       regex: /^merchants\/(\S+)\/orders\/(\S+)$/i,
  //       expect: [':merchant_id', ':order_id'],
  //       selectionRange:[12, 24],
  //       template: 'merchants / :merchant_id / orders / :order_id',
  //       description: 'View'
  //     },  {
  //       category: 'Gateways',
  //       regex: /^merchants\/(\S+)\/gateways$/i,
  //       selectionRange: [12, 24],
  //       expect: [':merchant_id'],
  //       template: `merchants / :merchant_id / gateways`,
  //       description: 'List'
  //     }, {
  //       category: 'Gateways',
  //       regex: /^merchants\/(\S+)\/gateways\/(\S+)\/configure$/i,
  //       expect: [':merchant_id', ':gateway_id'],
  //       template: `merchants / :merchant_id / gateways / :gateway_id / configure`,
  //       description: 'New Gateway'
  //     }, {
  //       category: 'Gateways',
  //       regex: /^merchants\/(\S+)\/gateways\/([0-9]{1})\/settings$/i,
  //       expect: [':merchant_id', ':gateway_id'],
  //       selectionRange: [12, 24],
  //       template: `merchants / :merchant_id / gateways / :gateway_id / settings`,
  //       description: 'View'
  //     },  {
  //       category: 'Resellers',
  //       regex: /^resellers\/(\S+)\/summary$/i,
  //       template: `resellers / ${resellerId} / summary`,
  //       description: 'Account Summary'
  //     }];
  //
  //     items.push(...resellerItems);
  //   }
  //
  //   items.push({
  //     category: 'My Account',
  //     regex: /^myaccounnt\/my_details$/i,
  //     template: 'myaccount/my_details',
  //     description: 'My Details'
  //   });
  //
  //   // Remove refunds from menu for now.
  //   items = R.pipe(
  //     R.reject(R.propEq('category', 'Refunds')),
  //     R.uniqBy(R.prop('template'))
  //   )(items);
  //
  //   return items;
  // }

  loadHome() {
    let location = "sample"
    dispatchNavigationEvent(location);
  }

  componentDidMount() {
    this.loadHome();
  }

  render() {
    return (
      <div > {this.props.children}
        </div>
    )
  }
}
