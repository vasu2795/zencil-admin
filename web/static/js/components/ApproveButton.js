import React, { Component } from 'react';
import PropTypes from 'prop-types'
import { connect } from 'react-redux';
import FlatButton from 'material-ui/FlatButton';
import { showNotification as showNotificationAction } from 'admin-on-rest';
import { push as pushAction } from 'react-router-redux';
import ActionDone from 'material-ui/svg-icons/action/done';

class ApproveButton extends Component {
    constructor(props) {
      super(props)
      this.props = props
    }
    handleClick = () => {
        const { push, record, showNotification } = this.props;
        const updatedRecord = { ...record, is_approved: true };
        fetch(`/approve/${record.id}`, { method: 'PUT', body: updatedRecord })
            .then(() => {
                showNotification('Successful');
                push('/approve_post');
            })
            .catch((e) => {
                console.error(e);
                showNotification('Error: Not Successful', 'warning')
            });
    }

    render() {
        return <FlatButton primary label={this.props.label ? this.props.label : "Approve"}  onClick={this.handleClick} />;
    }
}

ApproveButton.propTypes = {
    push: PropTypes.func,
    record: PropTypes.object,
    showNotification: PropTypes.func,
};

export default connect(null, {
    showNotification: showNotificationAction,
    push: pushAction,
})(ApproveButton);
