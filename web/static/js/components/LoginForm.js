import React from 'react';
import authClient from './authClient';
import PostIcon from 'material-ui/svg-icons/action/book';
import UserIcon from 'material-ui/svg-icons//social/group';
import { Delete } from 'admin-on-rest/lib/mui';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import { jsonServerRestClient, fetchUtils,Admin, Resource } from 'admin-on-rest';

import { PublishedPostList,PublishedPostEdit } from './PublishedPosts';
import { PendingPostList,PendingPostEdit } from './PendingPosts';
import { DeletedPostList,DeletedPostEdit } from './DeletedPosts';
import { DiscardedPostList,DiscardedPostEdit } from './DiscardedPosts';
import { UserList,UserEdit } from './Users';


const restClient = jsonServerRestClient('https://young-plateau-47693.herokuapp.com');


export default class LoginForm extends React.Component {
 render() {
   console.log("qwerty2....")
   return (
     <MuiThemeProvider >
     <Admin title="Zencil Admin" authClient={authClient} restClient={restClient}>
            <Resource options={{ label: 'Published Posts' }} name="published_posts" list={PublishedPostList} edit={PublishedPostEdit} icon={PostIcon} remove={Delete} />
            <Resource options={{ label: 'Pending Posts' }} name="pending_posts" list={PendingPostList} edit={PendingPostEdit} icon={PostIcon} remove={Delete} />
            <Resource options={{ label: 'Deleted Posts' }} name="deleted_posts" list={DeletedPostList} edit={DeletedPostEdit} icon={PostIcon} remove={Delete} />
            <Resource options={{ label: 'Discarded Posts' }} name="discarded_posts" list={DiscardedPostList} edit={DiscardedPostEdit} icon={PostIcon} remove={Delete} />
            <Resource options={{ label: 'Users' }} name="users" list={UserList} edit={UserEdit} icon={UserIcon} />
     </Admin>
     </MuiThemeProvider>
   )
 }
}
