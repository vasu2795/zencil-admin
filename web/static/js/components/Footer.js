import React from 'react';

export default class Footer extends React.Component {
  constructor(props) {
    super(props);
    this.props = props;
  }

  render() {
    return (
      <div style={{'position': 'absolute','left': '11.72%','right': '15.82%', 'bottom': '0px','height':'85px'}}>
        <hr style={{'border': '1px solid #DCDCDC','top': '0px'}}/>
        <div style={{'display': 'flex','text-align': 'center','font-family': 'Avenir-Light','font-size': '16px','color': '#999999','letter-spacing': '0.8px'}}>
          <div style={{'display': 'inline-block','margin-left':'7.42%'}}>About us</div>
          <div style={{'display': 'inline-block','padding-left':'3.9%'}}>Contact</div>
        </div>
      </div>
    );
  }
}
