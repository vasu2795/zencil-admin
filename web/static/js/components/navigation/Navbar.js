// import React from 'react';
// import axios from 'axios';
// import R from 'ramda';
// import Avatar from 'react-avatar';
//
// import { hashHistory } from 'react-router';
// import store from '../../store/AppStore';
// import { teardownApp } from '../../actions/AuthActions';
// import dispatchNavigationEvent from '../../RoutingManagement';
// import socket from '../../Notifications';
// /**
//  * Notifications
//  */
//
//  const MediumText = (props) => (
//      <div style={{'font-family': ' Avenir-Book','margin-top': '20px', 'margin-bottom': '20px','height':'20px','fontSize': '13px','opacity':  '1','color':  '#999999','textAlign': 'left','letterSpacing': '0.8px'}} >{props.text}</div>
//  );
//
//  export default class Navbar extends React.Component {
//     constructor(props) {
//       super(props);
//       this.props = props;
//     }
//
//     componentDidMount() {
//       console.log("here.... in navbar")
//       this.setState(this.state);
//     }
//
//     logout() {
//       axios.get('/auth/logout').then((response) => {
//         if(response.data.status === 'logged_out') {
//           sessionStorage.clear();
//           store.dispatch(teardownApp());
//           hashHistory.push('/login')
//         }
//       }).catch((err) => {
//         console.error(err);
//       });
//     }
//
//     render() {
//         const imageUrl = "./zencil.png"
//         return (
//           <div id="topBar" className="ui top fixed menu" style={{'height': '60px', 'zIndex': '10000000'}}>
//             <div style={{'width' : '20%', 'height': '100%'}}>
//               <div style={{'padding-left':'7.81%','padding-top':'20px','padding-bottom':'20px'}}>
//                 <img src="images/zencil.png"/>
//               </div>
//             </div>
//             <div style={{'text-align':'center','marginLeft': '20%','height': '100%'}} className="vr">&nbsp;</div>
//             <div style={{'text-align':'center','display':'flex','position' : 'absolute','marginLeft':'20%','height': '100%', 'width': '80%'}}>
//               <div style = {{'width': '22.21%','height': '100%','display':'inline-block', 'margin-left': '3.75%','display': 'inline-block'}} >
//                 <div style={{'display':'inline-block','height':'100%','width':'47%'}}><MediumText text="Trending" /></div>
//                 <div style={{'display':'inline-block','height':'100%','width':'35%'}}><MediumText text="Latest" /></div>
//                 <div style={{'display':'inline-block','height':'100%','width':'18%'}}><MediumText text="A-Z" /></div>
//               </div>
//               <div style={{'display':'inline-block','height':'100%','margin-left': '25.375%'}}><MediumText text="Write a story"/></div>
//               <div style={{'display':'flex','width':'26.85%','height':'100%','marginLeft': '4.625%'}}>
//                   <div style={{'display':'inline-block','marginTop': '10px','width':'81.8%','marginBottom': '10px','height':'40px','opacity': '0.25','background': '#D8D8D8','border-radius': '3px 0px 0px 3px'}}><input style={{'font-family': ' Avenir-Book','fontSize': '16px','opacity':  '1','color':  'black','textAlign': 'left','letterSpacing': '0.8px','border':'none','background':'transparent','width': '100%','height': '100%','padding-left':'15px'}}  placeholder={"Search"} type='text' /></div>
//                   <div style={{'display':'inline-block','marginTop': '10px','width':'18.2%','marginBottom': '10px','height':'40px','background': '#D8D8D8','border-radius': '0px 3px 3px 0px'}}></div>
//               </div>
//               <div style={{'display':'inline-block','height':'100%','margin-left': '2.924%'}}>
//                 <div  className="round" style={{'background': '#D8D8D8','width':'40px','height':'40px'}}>
//                 <Avatar size="40" round='true' facebook-id="invalidfacebookusername" src="http://www.gravatar.com/avatar/a16a38cdfe8b2cbd38e8a56ab93238d3" />
//                 </div>
//               </div>
//             </div>
//          </div>);
//     }
// }
