import React from 'react';
import R from 'ramda';

import Navbar from './navigation/Navbar';
import Footer from './Footer'
import dispatchNavigationEvent from '../RoutingManagement';

export default class Preview extends React.Component {
  constructor(props) {
    super(props);
  }


  loadHome() {
    console.log("loading home")
    // dispatchNavigationEvent("ar");
  }

  componentDidMount() {
    console.log("here...")
    this.loadHome();
  }

  render() {
    return (
      <div>
        <Navbar />
        <Footer />
      </div>
    )
  }
}
