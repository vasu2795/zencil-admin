import React from 'react';
import { Filter,List,Edit,SimpleForm,DisabledInput,ReferenceInput, SelectInput, RadioButtonGroupInput,TextInput,LongTextInput,Datagrid, TextField,EditButton } from 'admin-on-rest/lib/mui';
import { CardActions } from 'material-ui/Card';
import FlatButton from 'material-ui/FlatButton';

import { ListButton, ShowButton, DeleteButton, ChipField } from 'admin-on-rest/lib/mui';
import ActionVisibility from 'material-ui/svg-icons/action/visibility';
import ApproveButton from './ApproveButton';

const cardActionStyle = {
    zIndex: 2,
    display: 'inline-block',
    float: 'right',
};

const UserFilter = (props) => (
    <Filter {...props}>
        <TextInput label="Search" source="q" alwaysOn />
        <TextInput label="Title" source="title" defaultValue="Hello, World!" />
    </Filter>
);

const UserEditActions = ({ basePath, data, refresh }) => (
    <CardActions style={cardActionStyle}>
        <ListButton basePath={basePath} />
        <DeleteButton  basePath={basePath} record={data} />
        <FlatButton primary label="Refresh" onClick={refresh}   />
        <FlatButton primary  label="Preview" onClick={() => { console.log("clicking",data);  window.open(`#/preview/${data.id}`,'_blank');}} ></FlatButton>
    </CardActions>
);

export const UserList = (props) => (
    <List {...props} filters={<UserFilter />} >
        <Datagrid>
            <TextField source="id" />
            <TextField source="username" />
            <TextField source="role" />
            <EditButton />
        </Datagrid>
    </List>
);

const UserTitle = ({ record }) => {
    return <span>User {record ? `"${record.username}"` : ''}</span>;
};

export const UserEdit = (props) => (
    <Edit title={<UserTitle />} actions={<UserEditActions />} {...props}>
        <SimpleForm>
            <DisabledInput source="id" />
            <DisabledInput source="username" />
        </SimpleForm>
    </Edit>
);
