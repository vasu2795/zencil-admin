import { createStore, combineReducers } from 'redux';
import { appStatus, merchantList, gateways } from './Application';
import navigateTo from './Navigation';

// Combine reducers and create te store

function configureStore() {
  const store = createStore(
    combineReducers({
      'Application': appStatus,
      'Navigation': navigateTo,
      'Merchants': merchantList
    }),
    {
      Application: {
        appStatus: null
      },
      Merchants: {},
      Navigation: {
        lastVisited: ''
      }
    }
  );

  if (module.hot) {
    // Enable Webpack hot module replacement for reducers
    module.hot.accept('../reducers', () => {
      const nextRootReducer = require('../reducers/index');
      store.replaceReducer(nextRootReducer);
    });
  }

  return store;
}


let store =  configureStore()
export default store;
