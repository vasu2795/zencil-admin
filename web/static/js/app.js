import ReactDOM from 'react-dom';
import React from 'react';
import axios from 'axios';

import store from './store/AppStore';
import { initApp } from './actions/AuthActions';

import LoginForm from './components/LoginForm';
import ResetForm from './components/ResetForm';
import MainApp from './components/MainApp';
import Preview from './components/Preview'

import { Router, Route, IndexRoute, hashHistory } from 'react-router';


axios.defaults.headers.common['Version'] = '2016-07-19';
let current_url=window.location.href;
console.log("current url",current_url)
if(!current_url.includes("/preview") ){
  let loginStatus = localStorage.getItem('username');
  if(loginStatus) {
    hashHistory.push('/');
  } else {
    console.log("heres")
    hashHistory.push('/login');
  }
}

hashHistory.listen((args) => {
  let simplePath = args.pathname.split('/').join('');
  let currentSimplePath = store.getState()['Navigation'].lastVisited.split('/').join('');
  console.log("args url",currentSimplePath)
  if(simplePath != currentSimplePath) {
    console.log("happening")

		store.dispatch({
      type: 'NAVIGATION_ACTION',
      to: args.pathname
    });
  }
});

ReactDOM.render(
	<Router history={hashHistory}>
    <Route path="/login" component={LoginForm} />
    <Route path="/" component={LoginForm} />
    <Route path="/preview/:id" component={Preview} />
	</Router>, document.getElementById('application'));
