
defmodule ZencilAdmin.UserController do
  use ZencilAdmin.Web, :controller
  import Plug.Conn
  import Ecto.Query
  import Ecto.Changeset

  def list_users(conn, _params) do
    users = ZencilAdmin.User
                |> ZencilAdmin.Post.sorted(String.to_atom(_params["_sort"]))
                |> Repo.all
                |> Enum.map(fn x -> x end)

    users = case _params["_order"] do
              "DESC" -> Enum.reverse(users)
              _-> users
            end
    conn = conn
            |> Plug.Conn.put_resp_header("Access-Control-Allow-Origin", "*")
            |> Plug.Conn.put_resp_header("X-Total-Count",to_string(length(users)))
    render conn , users: users

  end

  def get_user(conn, %{"id" => id}) do
    users = ZencilAdmin.User
                |> where(id: ^id)
                |> Repo.one
    IO.inspect users
    conn = conn
            |> Plug.Conn.put_resp_header("Access-Control-Allow-Origin", "*")
            |> Plug.Conn.put_resp_header("X-Total-Count","1")
    json conn, users

  end

end
