defmodule ZencilAdmin.PostController do
  use ZencilAdmin.Web, :controller
  import Plug.Conn
  import Ecto.Query
  import Ecto.Changeset

  def list_published_posts(conn, _params) do
    status = "published"
    posts = ZencilAdmin.Post
                |> where(status: ^status)
                |> ZencilAdmin.Post.sorted(String.to_atom(_params["_sort"]))
                |> Repo.all
                |> Enum.map(fn x -> x end)

    posts = case _params["_order"] do
              "DESC" -> Enum.reverse(posts)
              _-> posts
            end
    conn = conn
            |> Plug.Conn.put_resp_header("Access-Control-Allow-Origin", "*")
            |> Plug.Conn.put_resp_header("X-Total-Count",to_string(length(posts)))
    render conn , posts: posts
  end

  def list_pending_posts(conn, _params) do
    status = "pending"
    posts = ZencilAdmin.Post
                |> where(status: ^status)
                |> ZencilAdmin.Post.sorted(String.to_atom(_params["_sort"]))
                |> Repo.all
                |> Enum.map(fn x -> x end)
    posts = case _params["_order"] do
              "DESC" -> Enum.reverse(posts)
              _-> posts
            end
    conn = conn
            |> Plug.Conn.put_resp_header("Access-Control-Allow-Origin", "*")
            |> Plug.Conn.put_resp_header("X-Total-Count",to_string(length(posts)))
    render conn , posts: posts
  end

  def list_deleted_posts(conn, _params) do
    status = "deleted"
    posts = ZencilAdmin.Post
                |> where(status: ^status)
                |> ZencilAdmin.Post.sorted(String.to_atom(_params["_sort"]))
                |> Repo.all
                |> Enum.map(fn x -> x end)
    posts = case _params["_order"] do
              "DESC" -> Enum.reverse(posts)
              _-> posts
            end
    conn = conn
            |> Plug.Conn.put_resp_header("Access-Control-Allow-Origin", "*")
            |> Plug.Conn.put_resp_header("X-Total-Count",to_string(length(posts)))
    render conn , posts: posts
  end

  def list_discarded_posts(conn, _params) do
    status = "discarded"
    posts = ZencilAdmin.Post
                |> where(status: ^status)
                |> ZencilAdmin.Post.sorted(String.to_atom(_params["_sort"]))
                |> Repo.all
                |> Enum.map(fn x -> x end)
    posts = case _params["_order"] do
              "DESC" -> Enum.reverse(posts)
              _-> posts
            end
    conn = conn
            |> Plug.Conn.put_resp_header("Access-Control-Allow-Origin", "*")
            |> Plug.Conn.put_resp_header("X-Total-Count",to_string(length(posts)))
    render conn , posts: posts
  end

  def get_post(conn, %{"id" => id}) do
    posts = ZencilAdmin.Post
                |> where(id: ^id)
                |> Repo.one
    IO.inspect posts
    conn = conn
            |> Plug.Conn.put_resp_header("Access-Control-Allow-Origin", "*")
            |> Plug.Conn.put_resp_header("X-Total-Count","1")
    json conn, posts
  end

  def delete_post(conn, %{"id" => id}) do
    post = Repo.get ZencilAdmin.Post,id

    {status, data} = post
      |> cast(%{"status": "deleted"}, [:status])
      |> Repo.update

    IO.inspect data
    conn = conn
            |> Plug.Conn.put_resp_header("Access-Control-Allow-Origin", "*")
            |> Plug.Conn.put_resp_header("X-Total-Count","1")
    json conn, %{}
  end

  def discard_post(conn, %{"id" => id}) do
    post = Repo.get ZencilAdmin.Post,id

    {status, data} = post
      |> cast(%{"status": "discarded"}, [:status])
      |> Repo.update

    IO.inspect data
    conn = conn
            |> Plug.Conn.put_resp_header("Access-Control-Allow-Origin", "*")
            |> Plug.Conn.put_resp_header("X-Total-Count","1")
    json conn, %{}
  end

  def approve_post(conn, %{"id" => id}) do
    post = Repo.get ZencilAdmin.Post,id

    {status, data} = post
                      |> cast(%{"status": "published", "views": 0, "shares": 0}, [:status, :views, :shares])
                      |> Repo.update

    IO.inspect data
    conn = conn
            |> Plug.Conn.put_resp_header("Access-Control-Allow-Origin", "*")
            |> Plug.Conn.put_resp_header("X-Total-Count","1")
    json conn, %{}
  end

end
