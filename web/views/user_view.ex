defmodule ZencilAdmin.UserView do
  use ZencilAdmin.Web, :view

  def render("list_users.json", %{ users: users }) do
    users
  end

end
