defmodule ZencilAdmin.Post do
  use ZencilAdmin.Web, :model
  use Ecto.Schema
  import Ecto.Query
  import Ecto.Changeset


  alias ZencilAdmin.{Repo}
  @derive{Poison.Encoder, only: [:id, :title, :category, :coverpic, :filelocation, :status, :user_id, :date_created, :last_updated,:username, :views, :shares ]}
  schema "posts" do
    field :title, :string
    field :category, :string
    field :coverpic, :string
    field :filelocation, :string
    field :status, :string
    field :user_id, :integer
    field :username, :string
    field :views, :integer
    field :shares, :integer
    timestamps([{:inserted_at, :date_created}, {:updated_at, :last_updated}])
  end

  def sorted(query,param) do
    IO.inspect "params "
    IO.inspect param
    direction = :desc
    from p in query,
    order_by: [{^direction, field(p, ^param)}]
  end

end
