# create table users(id serial PRIMARY KEY,
#                      username varchar(255),
#                     password varchar(255),
#                     age int,
#                     location varchar(255),
#                     occupation varchar(255),
#                     role varchar(16),
#                     email_id varchar(255) not null
#                     );

defmodule ZencilAdmin.User do
  use ZencilAdmin.Web, :model
  use Ecto.Schema
  import Ecto.Query
  import Ecto.Changeset


  alias ZencilAdmin.{Repo}
  @derive{Poison.Encoder, only: [:id, :username, :password, :age, :location, :occupation, :role, :email_id]}
  schema "users" do
    field :username, :string
    field :password, :string
    field :occupation, :string
    field :location, :string
    field :role, :string
    field :age, :integer
    field :email_id, :string
  end

  def sorted(query,param) do
    IO.inspect "params "
    IO.inspect param
    direction = :desc
    from p in query,
    order_by: [{^direction, field(p, ^param)}]
  end

end
